import sys

import requests


def get_package_versions(version):
    url = "https://gitlab.com/api/v4/projects/27986255/packages"
    r = requests.get(url)
    package_info = [package for package in r.json() if package["version"] == version]
    if len(package_info) > 1:
        sys.stdout.write(str(package_info[0]["id"]))
    else:
        sys.stdout.write("-1")


if __name__ == "__main__":
    version_hash = sys.argv[1]
    get_package_versions(version_hash)
    sys.exit(0)
