#!/bin/bash

git clone https://github.com/EMPAIA/openslide.git
cd openslide
git checkout $1
autoreconf -i
./configure
make
curl --header "JOB-TOKEN: $2" --upload-file src/.libs/libopenslide.so.0.4.1 "$3/projects/$4/packages/generic/libopenslide.so.0/$1/libopenslide.so.0?select=package_file"
