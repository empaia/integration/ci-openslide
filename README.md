# CI OpenSlide

This repository is intended to build and upload a customized version of OpenSlide as a generic package to the GitLab package repository.

The corresponding source code of the customized OpenSlide can be found here: [https://github.com/EMPAIA/openslide](https://github.com/EMPAIA/openslide).

## Build and upload new version of OpenSlide

Steps needed to build and upload a new version of OpenSlide:

1. Go to [https://github.com/EMPAIA/openslide](https://github.com/EMPAIA/openslide) and choose a branch and commit you want to build.
2. Copy the hash of the commit you have chosen.
3. Replace the value of `OPENSLIDE_VERSION` in `openslide_version.env` with the copied commit hash.
4. Commit the changed `openslide_version.env` file.
5. When the changes are commited or merged to `main`, the specified version of OpenSlide will be build and the file `libopenslide.so.0` will be uploaded to the GitLab package repository. The commit hash from `openslide_version.env` will be used as version of the package.
**NOTE:** If the hash was already used to upload a version of `libopenslide.so.0` no new version will be build.
